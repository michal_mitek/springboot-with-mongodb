package com.example.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.mongobee.Mongobee;

@Configuration
public class MongoDbConfig {
	
	@Bean
	public Mongobee mongobee(){
		Mongobee runner = new Mongobee("mongodb://DESKTOP-IN902S0:27017/RestaurantDb");
		runner.setDbName("RestaurantDb");
		runner.setChangeLogsScanPackage("com.example.config");
		runner.setEnabled(true);
		System.out.println("-------------- ---    ---->>>>>>>>>>>><<<<<<<<<<<<---------------");
		return runner;
	}
	
}
