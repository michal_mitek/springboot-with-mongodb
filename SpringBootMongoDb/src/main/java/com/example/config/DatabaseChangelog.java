package com.example.config;

import org.bson.Document;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.client.MongoDatabase;

@ChangeLog(order = "001")
public class DatabaseChangelog {
	

	@ChangeSet(author = "michalMitek", id = "001", order = "001")
	public void setUpDb(MongoDatabase db){
		
		if(db.getCollection("Restaurants") == null){
			db.createCollection("Restaurants");
		}
		
		System.out.println("-------------- ---    ---->>>>>>>>>>>><<<<<<<<<<<<---------------");
		
		com.mongodb.client.MongoCollection<Document> users = db.getCollection("Restaurants");
		Document doc = new Document("email", "password").append("asds", "asdssws");
		users.insertOne(doc);
		
	}

}
