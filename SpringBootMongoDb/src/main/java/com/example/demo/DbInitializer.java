package com.example.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DbInitializer implements CommandLineRunner {

	private RestaurantRepository restaurantRepository;

	public DbInitializer(RestaurantRepository restaurantRepository) {
		this.restaurantRepository = restaurantRepository;
	}

	@Override
	public void run(String... args) throws Exception {
		Restaurant targowa = new Restaurant("Targowa", 250, new Address("wroclaw", "Poland"),
				Arrays.asList(new Review("Tomek", 8, true), new Review("Michal", 2, false)));

		Restaurant sphinx = new Restaurant("Sphinx", 100, new Address("Warsaw", "Poland"),
				Arrays.asList(new Review("Krzysiek", 7, false), new Review("Filip", 9, true)));

		Restaurant novokaina = new Restaurant("Novokaina", 150, new Address("Berlin", "Germany"), new ArrayList<>()

		);

		this.restaurantRepository.deleteAll();

		List<Restaurant> restaurants = Arrays.asList(targowa, sphinx, novokaina);
		this.restaurantRepository.save(restaurants);

	}

}
