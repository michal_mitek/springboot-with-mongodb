package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.querydsl.core.types.dsl.BooleanExpression;
@RestController
@RequestMapping("/restaurants")
public class RestaurantController{
	
	private RestaurantRepository restaurantRepository;
	
	@Autowired
	public RestaurantController(RestaurantRepository restaurantrepository) {
		this.restaurantRepository = restaurantrepository;
	}
	
	@GetMapping("/all")
	public List<Restaurant> getAll(){
		List<Restaurant> restaurants =  this.restaurantRepository.findAll();
		return restaurants;
	}
	
	@PutMapping
	public void insert(@RequestBody Restaurant restaurant){
		this.restaurantRepository.insert(restaurant);
	}
	
	@PostMapping
	public void save(@RequestBody Restaurant restaurant){
		this.restaurantRepository.save(restaurant);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable("id") String id){
		this.restaurantRepository.delete(id);
	}
	
	@GetMapping("/{id}")
    public Restaurant getById(@PathVariable String id) throws RestaurantNotFoundException{
        Restaurant restaurant = this.restaurantRepository.findById(id);
        return restaurant;
    }

    @GetMapping("/price/{maxPrice}")
    public List<Restaurant> getByPricePerDinner(@PathVariable("maxPrice") int maxPrice){
        List<Restaurant> restaurants = this.restaurantRepository.findByPricePerDinnerLessThan(maxPrice);
        return restaurants;
    }

    @GetMapping("/address/{city}")
    public List<Restaurant> getByCity(@PathVariable("city") String city){
        List<Restaurant> restaurants = this.restaurantRepository.findByCity(city);
        return restaurants;
    }

    @GetMapping("/country/{country}")
    public List<Restaurant> getByCountry(@PathVariable("country") String country){
        QRestaurant qRestaurant = new QRestaurant("restaurant");
        BooleanExpression filterByCountry = qRestaurant.address.country.eq(country);

        List<Restaurant> restaurants = (List<Restaurant>) this.restaurantRepository.findAll(filterByCountry);
        return restaurants;
    }

    @GetMapping("/recommended")
    public List<Restaurant> getRecommended(){
        final int maxPrize = 300;
        final int minRating = 7;

        QRestaurant qRestaurant = new QRestaurant("restaurant");

        BooleanExpression filterByPrice = qRestaurant.pricePerDinner.lt(maxPrize);
        BooleanExpression filterByRating = qRestaurant.reviews.any().rating.gt(minRating);

        List<Restaurant> restaurants = (List<Restaurant>) this.restaurantRepository.findAll(filterByPrice.and(filterByRating));
        return restaurants;
    }
}