package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Restaurants")
public class Restaurant {

	@Id
	private String id;
	private String name;
	@Indexed(direction = IndexDirection.ASCENDING)
	private int pricePerDinner;
	private Address address;
	private List<Review> reviews;
	
	protected Restaurant() {
		this.reviews = new ArrayList<>();
	}

	public Restaurant(String name, int price, Address address, List<Review> reviews) {
		this.name = name;
		this.pricePerDinner = price;
		this.address = address;
		this.reviews = reviews;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getPrice() {
		return pricePerDinner;
	}

	public Address getAddress() {
		return address;
	}

	public List<Review> getReviews() {
		return reviews;
	}
	
}
