package com.example.demo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RestaurantRepository extends MongoRepository<Restaurant, String>, QueryDslPredicateExecutor<Restaurant> {
	
	Restaurant findById(String id);
	
	List<Restaurant> findByPricePerDinnerLessThan(int maxPrice);
	
	@Query(value = "{address.city:?0}")
	List<Restaurant> findByCity(String city);
}
