package com.example.demo;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


@RunWith(SpringRunner.class)
@SpringBootConfiguration
@WebAppConfiguration
public class SpringBootMongoDbApplicationTests {

	private MockMvc mockMvc;
	
	@Mock
	private RestaurantController restaurantController;
	
	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(restaurantController).build();
	}
	
	@Test
	public void validateGetAll() throws Exception{
		
		Restaurant targowa = new Restaurant("Targowa", 250, new Address("wroclaw", "Poland"),
				Arrays.asList(new Review("Tomek", 8, true), new Review("Michal", 2, false)));

		Restaurant sphinx = new Restaurant("Sphinx", 100, new Address("Warsaw", "Poland"),
				Arrays.asList(new Review("Krzysiek", 7, false), new Review("Filip", 9, true)));

		List<Restaurant> restaurants = Arrays.asList(targowa, sphinx);
		
		when(restaurantController.getAll()).thenReturn(restaurants);
		
		mockMvc.perform(get("http://localhost:8080/restaurants/all")).andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$[0].name", is("Targowa")))
		.andExpect(jsonPath("$[0].price", is(250)))
		.andExpect(jsonPath("$[0].address.city", is("wroclaw")))
		.andExpect(jsonPath("$[0].address.country", is("Poland")))
		.andExpect(jsonPath("$[0].reviews[0].userName", is("Tomek")))
		.andExpect(jsonPath("$[0].reviews[0].rating", is(8)))
		.andExpect(jsonPath("$[0].reviews[0].approved", is(true)))
		.andExpect(jsonPath("$[0].reviews[1].userName", is("Michal")))
		.andExpect(jsonPath("$[0].reviews[1].rating", is(2)))
		.andExpect(jsonPath("$[0].reviews[1].approved", is(false)))
		.andExpect(jsonPath("$[1].name", is("Sphinx")))
		.andExpect(jsonPath("$[1].price", is(100)))
		.andExpect(jsonPath("$[1].address.city", is("Warsaw")))
		.andExpect(jsonPath("$[1].address.country", is("Poland")))
		.andExpect(jsonPath("$[1].reviews[0].userName", is("Krzysiek")))
		.andExpect(jsonPath("$[1].reviews[0].rating", is(7)))
		.andExpect(jsonPath("$[1].reviews[0].approved", is(false)));
	}
	
	
}
